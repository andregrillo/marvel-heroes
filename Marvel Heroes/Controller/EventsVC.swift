//
//  EventsVC.swift
//  Marvel Heroes
//
//  Created by André Grillo on 23/07/2019.
//  Copyright © 2019 André Grillo. All rights reserved.
//

import UIKit

class EventsVC: UIViewController {
    @IBOutlet weak var eventsLbl: UILabel!
    var heroID: Int = 0 {
        didSet {
            let downloadHeroDetails = HeroesDownloaderAndParser()
            downloadHeroDetails.downloadEventsData(forHeroID: heroID) { events in
                self.updateUI(events: events)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //Coloca o Loader
        DispatchQueue.main.async {
            Loader.instance.showLoader()
        }
    }
    
    func updateUI(events: [Event]) {
        DispatchQueue.main.async {
            if events.count == 0 {
                self.eventsLbl.text = "No Events available for this character on Marvel API"
            }
            for event in events {
                self.eventsLbl.text?.append(contentsOf: "Event: \(event.title!)\n")
                self.eventsLbl.text?.append(contentsOf: "Description: \(event.description!)\n\n")
            }
            Loader.instance.hideLoader()
        }
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
