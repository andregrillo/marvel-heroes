//
//  MainVC.swift
//  Marvel Heroes
//
//  Created by André Grillo on 21/07/2019.
//  Copyright © 2019 André Grillo. All rights reserved.
//

import UIKit
import AVFoundation

class MainVC: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    let downloaderAndParser = HeroesDownloaderAndParser()
    var heroesArray = [Character]()
    var filteredHeroArray = [Character]()
    var queryText = ""
    var inSearchMode = false
    var musicPlayer: AVAudioPlayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.search
        collectionView.keyboardDismissMode = .onDrag
        
        //Recupera as informações dos heróis favoritos no UserDefaults
        let defaults = UserDefaults.standard
        if let items = defaults.array(forKey: "Favorites") as? [Int] {
            favorites = items
        }
        
        //Faz o download dos personagens e inicia o CollectionView
        downloaderAndParser.downloadData(offset: 0) { characters in
            self.heroesArray = characters
            self.updateUI()
        }
        
        initAudio()
    }
    
    func updateUI() {
        isDowloadingJSON = false
        DispatchQueue.main.async {
            if isDowloadingImage == false {
                Loader.instance.hideLoader()
            }
            self.collectionView.reloadData()
        }
    }
    
    //MARK: - Audio Related Methods
    
    func initAudio() {
        let path = Bundle.main.url(forResource: "music", withExtension: "mp3")
        
        do {
            musicPlayer = try AVAudioPlayer(contentsOf: path!)
            musicPlayer.prepareToPlay()
            musicPlayer.numberOfLoops = -1
            musicPlayer.play()
            
        } catch {
            print(error.localizedDescription)
        }
        
    }
    
    @IBAction func musicBtnPressed(_ sender: UIButton) {
        if musicPlayer.isPlaying {
            musicPlayer.pause()
            sender.alpha = 0.4
        } else {
            musicPlayer.play()
            sender.alpha = 1.0
        }
    }
    
    //MARK: - Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "HeroVC" {
            let destinationVC = segue.destination as! HeroVC
            
            let iPath = self.collectionView.indexPathsForSelectedItems
            let indexPath : NSIndexPath = iPath![0] as NSIndexPath
            let rowIndex = indexPath.row
            
            //Verifica qual o Array está em uso
            if inSearchMode {
                destinationVC.hero = filteredHeroArray[rowIndex]
            } else {
                destinationVC.hero = heroesArray[rowIndex]
            }
        }
    }
    
}

extension MainVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UISearchBarDelegate {
    
    //MARK: - UICollectionView Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if inSearchMode {
            return filteredHeroArray.count
        } else {
            return heroesArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HeroCell", for: indexPath) as? HeroCell {
            
            let hero: Character!
            if inSearchMode {
                hero = filteredHeroArray[indexPath.row]
            } else {
                hero = heroesArray[indexPath.row]
            }
            
            cell.configureCell(hero: hero)
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if !inSearchMode {
            if indexPath.row == heroesArray.count - 1 {
                //Última célula, carregar mais!
                
                //Mostra o Loader
                isDowloadingJSON = true
                if !isDowloadingImage {
                    DispatchQueue.main.async {
                        Loader.instance.showLoader()
                    }
                }
                
                //Verifica se ainda existem heróis disponíveis na API
                if heroesArray.count < downloaderAndParser.totalEntries {
                    //Chamar a API e baixar mais 20
                    downloaderAndParser.downloadData(offset: downloaderAndParser.fetchedIndex) { characters in
                        self.heroesArray.append(contentsOf: characters)
                        self.updateUI()
                    }
                }
                
            }
        } else {
            if indexPath.row == filteredHeroArray.count - 1 {
                
                if filteredHeroArray.count < downloaderAndParser.totalEntries {
                    //Chamar a API e baixar mais 20
                    downloaderAndParser.downloadSearchData(searchingText: queryText, withOffSet: downloaderAndParser.fetchedIndex) { characters in
                        self.filteredHeroArray.append(contentsOf: characters)
                        self.updateUI()
                    }
                } else {
                    isDowloadingJSON = false
                    DispatchQueue.main.async {
                        Loader.instance.hideLoader()
                    }
                }
            }   
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 125)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "HeroVC", sender: self)
    }
    
    //MARK: - UISearchBar Methods
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text!.isEmpty || searchBar.text == "" {
            queryText = ""
            downloaderAndParser.fetchedIndex = 0
            searchBar.perform(#selector(self.resignFirstResponder), with: nil, afterDelay: 0.1)
            if inSearchMode {
                inSearchMode = false
                filteredHeroArray = []
                heroesArray = []
                downloaderAndParser.downloadData(offset: 0) { characters in
                    self.inSearchMode = false
                    DispatchQueue.main.async {
                        self.collectionView.contentOffset = CGPoint(x: 0, y: 0)
                    }
                    self.heroesArray = characters
                    self.updateUI()
                }
            }
        } else {
            inSearchMode = true
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
         if !searchBar.text!.isEmpty {
            filteredHeroArray = []
            downloaderAndParser.fetchedIndex = 0
            inSearchMode = true
            queryText = searchBar.text!.lowercased()
            
            downloaderAndParser.downloadSearchData(searchingText: queryText, withOffSet: downloaderAndParser.fetchedIndex) { characters in
                self.filteredHeroArray = characters
                self.updateUI()
            }
            collectionView.reloadData()
        } else {
            inSearchMode = false
        }
    }
}

