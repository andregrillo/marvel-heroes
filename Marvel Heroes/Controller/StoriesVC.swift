//
//  StoriesVC.swift
//  Marvel Heroes
//
//  Created by André Grillo on 23/07/2019.
//  Copyright © 2019 André Grillo. All rights reserved.
//

import UIKit

class StoriesVC: UIViewController {

    @IBOutlet weak var storiesLbl: UILabel!
    var heroID: Int = 0 {
        didSet {
            let downloadHeroDetails = HeroesDownloaderAndParser()
            downloadHeroDetails.downloadStoriesData(forHeroID: heroID) { stories in
                self.updateUI(stories: stories)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //Coloca o Loader
        DispatchQueue.main.async {
            Loader.instance.showLoader()
        }
    }

    func updateUI(stories: [Story]) {
        DispatchQueue.main.async {
            if stories.count == 0 {
                self.storiesLbl.text = "No Stories available for this character on Marvel API"
            }
            for story in stories {
                self.storiesLbl.text?.append(contentsOf: "Story: \(story.title!)\n\n")
            }
            Loader.instance.hideLoader()
        }
    }

    @IBAction func backBtnPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
