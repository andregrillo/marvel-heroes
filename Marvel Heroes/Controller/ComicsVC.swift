//
//  ComicsVC.swift
//  Marvel Heroes
//
//  Created by André Grillo on 23/07/2019.
//  Copyright © 2019 André Grillo. All rights reserved.
//

import UIKit

class ComicsVC: UIViewController {

    @IBOutlet weak var comicsLbl: UILabel!
    var heroID: Int = 0 {
        didSet {
            let downloadHeroDetails = HeroesDownloaderAndParser()
            downloadHeroDetails.downloadComicsData(forHeroID: heroID) { comics in
                self.updateUI(comics: comics)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //Coloca o Loader
        DispatchQueue.main.async {
            Loader.instance.showLoader()
        }
    }
    
    func updateUI(comics: [Comic]) {
        DispatchQueue.main.async {
            if comics.count == 0 {
                self.comicsLbl.text = "No Comics available for this character on Marvel API"
            }
            for comic in comics {
                self.comicsLbl.text?.append(contentsOf: "Comic: \(comic.title!)\n")
                self.comicsLbl.text?.append(contentsOf: "ID: \(comic.id!)\n\n")
            }
            Loader.instance.hideLoader()
        }
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
