//
//  SeriesVC.swift
//  Marvel Heroes
//
//  Created by André Grillo on 23/07/2019.
//  Copyright © 2019 André Grillo. All rights reserved.
//

import UIKit

class SeriesVC: UIViewController {
    
    @IBOutlet weak var seriesLbl: UILabel!
    var heroID: Int = 0 {
        didSet {
            let downloadHeroDetails = HeroesDownloaderAndParser()
            downloadHeroDetails.downloadSeriesData(forHeroID: heroID) { series in
                self.updateUI(series: series)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //Coloca o Loader
        DispatchQueue.main.async {
            Loader.instance.showLoader()
        }
    }
    
    func updateUI(series: [Serie]) {
        DispatchQueue.main.async {
            if series.count == 0 {
                self.seriesLbl.text = "No Series available for this character on Marvel API"
            }
            for serie in series {
                self.seriesLbl.text?.append(contentsOf: "Serie: \(serie.title!)\n\n")
            }
            Loader.instance.hideLoader()
        }
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
