//
//  HeroVC.swift
//  Marvel Heroes
//
//  Created by André Grillo on 22/07/2019.
//  Copyright © 2019 André Grillo. All rights reserved.
//

import UIKit

class HeroVC: UIViewController {

    @IBOutlet weak var heroDetailImage: UIImageView!
    @IBOutlet weak var heroDescription: UILabel!
    @IBOutlet weak var heroNameLbl: UILabel!
    @IBOutlet weak var favoriteLbl: UILabel!
    
    var hero: Character?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let downloadHeroDetails = HeroesDownloaderAndParser()
        
        downloadHeroDetails.downloadHeroDetails(forHeroID: hero!.id!) { heroDetails in
            
            //Configura Imagem da Personagem
            //verifica se há imagem disponível
            if heroDetails.thumbnail!["path"]!.contains("image_not_available") || heroDetails.thumbnail!["path"] == nil || heroDetails.thumbnail!["path"] == "" {
                DispatchQueue.main.async {
                    self.heroDetailImage.image = UIImage(named: "descriptionPhotoNA.jpg")
                }
                
            } else {
                //Usa o FileManager para verificar se a imagem está no "cache"
                let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
                let url = NSURL(fileURLWithPath: path)
                if let pathComponent = url.appendingPathComponent("\(heroDetails.id!)-description.jpg") {
                    let filePath = pathComponent.path
                    let fileManager = FileManager.default
                    
                    //Verifica se a imagem já foi transferida. Se sim, será reutilizada.
                    if fileManager.fileExists(atPath: filePath) {
                        DispatchQueue.main.async {
                            self.heroDetailImage.image = UIImage(contentsOfFile: "\(path)/\(heroDetails.id!)-description.jpg")
                        }
                    }
                    else {
                        //Imagem não está no "Cache". Fazer então o download...
                        let imageStringURL = "\(heroDetails.thumbnail!["path"]!)/portrait_xlarge.\(heroDetails.thumbnail!["extension"]!)"
                        self.heroDetailImage.downloaded(from: imageStringURL, heroID: String(heroDetails.id!), isDescription: true)
                    }
                } else {
                    print("File Path not available")
                }
            }
            self.updateUI(heroDetails: heroDetails)
        }
    }
    
    func updateUI(heroDetails: HeroDetails) {
        isDowloadingJSON = false
        DispatchQueue.main.async {
            if heroDetails.description == "" {
                self.heroDescription.text = "Character description not available on Marvel API"
            } else {
                self.heroDescription.text = heroDetails.description
            }
            self.heroNameLbl.text = heroDetails.name
            self.favoriteLbl.text = (favorites.firstIndex(where: { $0 == heroDetails.id }) != nil) ? "Yes":"No"
            if !isDowloadingImage {
                Loader.instance.hideLoader()
            }
            
        }
        
    }
    
    @IBAction func backBtnPressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ComicsVC" {
            let destinationVC = segue.destination as! ComicsVC
            destinationVC.heroID = hero!.id!
        } else if segue.identifier == "StoriesVC" {
            let destinationVC = segue.destination as! StoriesVC
            destinationVC.heroID = hero!.id!
        } else if segue.identifier == "SeriesVC" {
            let destinationVC = segue.destination as! SeriesVC
            destinationVC.heroID = hero!.id!
        }
            else if segue.identifier == "EventsVC" {
            let destinationVC = segue.destination as! EventsVC
            destinationVC.heroID = hero!.id!
        }
    }
}
