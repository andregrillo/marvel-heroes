//
//  Marvel.swift
//  Marvel Heroes
//
//  Created by André Grillo on 21/07/2019.
//  Copyright © 2019 André Grillo. All rights reserved.
//

import Foundation

//Heroes
struct Character: Codable {
    let name: String?
    let id: Int?
    //let resourceURI: String?
    let thumbnail: Dictionary<String, String>?
    //var imageURL: String?
}

struct Dados: Codable {
    let characters: [Character]?
    let total: Int?
    
    enum CodingKeys: String, CodingKey {
        case characters = "results"
        case total
    }
}

struct Marvel: Codable {
    let attributionText: String?
    let data: Dados?
}

//Comics
struct Comic: Codable {
    let id: Int?
    let title: String?
    let urls: [Dictionary<String, String>?]?
}

struct ComicsData: Codable {
    let comicsArray: [Comic]?
    //let total: Int?
    
    enum CodingKeys: String, CodingKey {
        case comicsArray = "results"
        //case total
    }
}

struct MarvelComics: Codable {
    let data: ComicsData?
}

//Events
struct Event: Codable {
    let title: String?
    let description: String?
    let thumbnail: Dictionary<String, String>?
}

struct EventsData: Codable {
    let eventsArray: [Event]?
    
    enum CodingKeys: String, CodingKey {
        case eventsArray = "results"
    }
}

struct MarvelEvents: Codable {
    let data: EventsData?
}

//Series
struct Serie: Codable {
    let title: String?
    let startYear: Int?
    let endYear: Int?
    let thumbnail: Dictionary<String, String>?
}

struct SeriesData: Codable {
    let seriesArray: [Serie]?
    
    enum CodingKeys: String, CodingKey {
        case seriesArray = "results"
    }
}

struct MarvelSeries: Codable {
    let data: SeriesData?
}

//Stories
struct Story: Codable {
    let title: String?
}

struct StoriesData: Codable {
    let storiesArray: [Story]?
    
    enum CodingKeys: String, CodingKey {
        case storiesArray = "results"
    }
}

struct MarvelStories: Codable {
    let data: StoriesData?
}

//Hero
struct HeroDetails: Codable {
    let name: String?
    let id: Int?
    let description: String?
    let thumbnail: Dictionary<String, String>?
}

struct HeroData: Codable {
    let heroDetails: [HeroDetails]?
    
    enum CodingKeys: String, CodingKey {
        case heroDetails = "results"
    }
}

struct MarvelHero: Codable {
    let data: HeroData?
}
