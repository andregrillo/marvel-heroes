//
//  HeroesDownloaderAndParser.swift
//  Marvel Heroes
//
//  Created by André Grillo on 23/07/2019.
//  Copyright © 2019 André Grillo. All rights reserved.
//

import UIKit

class HeroesDownloaderAndParser {
    
    var totalEntries = 0
    var fetchedIndex = 0
    
    enum FetchType {
        case standard
        case search
        case comics
        case events
        case stories
        case series
        case hero
    }
    
    func prepareURL(fetchType: FetchType, withOffSet offSet: Int = 0, searchingText query: String = "", forHeroID heroID: Int = 0) -> URL{
        
        //Prepara as variáveis bases para a URL
        let baseURL = "https://gateway.marvel.com/v1/public/characters"
        let publicKey = "72b9beb20490fd86d07cab71dcd7307e"
        let privateKey = "4326bf042358bf19c4874b3cc0c970d2b3059fe2"
        let ts = NSDate().timeIntervalSince1970.description
        let hash = "\(ts)\(privateKey)\(publicKey)".md5Value
        var url = ""
        
        //Verifica e constrói os diversos tipos de Link
        switch fetchType {
        case .standard:
            url = "\(baseURL)?apikey=\(publicKey)&hash=\(hash)&ts=\(ts)&offset=\(offSet)&limit=20"
        case .search:
            url = "\(baseURL)?apikey=\(publicKey)&hash=\(hash)&ts=\(ts)&offset=\(offSet)&nameStartsWith=\(query)&limit=20"
        case .comics:
            url = "\(baseURL)/\(heroID)/comics?apikey=\(publicKey)&hash=\(hash)&ts=\(ts)&limit=3"
        case .events:
            url = "\(baseURL)/\(heroID)/events?apikey=\(publicKey)&hash=\(hash)&ts=\(ts)&limit=3"
        case .stories:
            url = "\(baseURL)/\(heroID)/stories?apikey=\(publicKey)&hash=\(hash)&ts=\(ts)&limit=3"
        case .series:
            url = "\(baseURL)/\(heroID)/series?apikey=\(publicKey)&hash=\(hash)&ts=\(ts)&limit=3"
        case .hero:
            url = "\(baseURL)/\(heroID)?apikey=\(publicKey)&hash=\(hash)&ts=\(ts)"
        }
        return URL(string: url)!
    }
    
    //MARK: - Métodos de downloaders & json parsers por Modelo de Dados
    //Comics
    func downloadComicsData(forHeroID heroID: Int, completion: @escaping ([Comic]) -> () ){
        let url = prepareURL(fetchType: .comics, forHeroID: heroID)
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            if let jsonData = data {
                let decoder = JSONDecoder()
                do {
                    let jsonPayload = try decoder.decode(MarvelComics.self, from: jsonData)
                    if let comics = jsonPayload.data?.comicsArray {
                        completion(comics)
                    }
                } catch {
                    print("There was an error parsing JSON Comics: \(error.localizedDescription)")
                }
            }
        }
        task.resume()
    }
    
    //Events
    func downloadEventsData(forHeroID heroID: Int, completion: @escaping ([Event]) -> () ){
        let url = prepareURL(fetchType: .events, forHeroID: heroID)
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            if let jsonData = data {
                let decoder = JSONDecoder()
                do {
                    let jsonPayload = try decoder.decode(MarvelEvents.self, from: jsonData)
                    if let events = jsonPayload.data?.eventsArray {
                        completion(events)
                    }
                } catch {
                    print("There was an error parsing JSON Events: \(error.localizedDescription)")
                }
            }
        }
        task.resume()
    }
    
    //Series
    func downloadSeriesData(forHeroID heroID: Int, completion: @escaping ([Serie]) -> () ){
        let url = prepareURL(fetchType: .series, forHeroID: heroID)
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            if let jsonData = data {
                let decoder = JSONDecoder()
                let jsonPayload: MarvelSeries!
                do {
                    jsonPayload = try decoder.decode(MarvelSeries.self, from: jsonData)
                    if let series = jsonPayload.data?.seriesArray {
                        completion(series)
                    }
                } catch {
                    print("There was an error parsing JSON Series: \(error.localizedDescription)")
                }
            }
        }
        task.resume()
    }
    
    //Stories
    func downloadStoriesData(forHeroID heroID: Int, completion: @escaping ([Story]) -> () ){
        let url = prepareURL(fetchType: .stories, forHeroID: heroID)
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            if let jsonData = data {
                let decoder = JSONDecoder()
                let jsonPayload: MarvelStories!
                do {
                    jsonPayload = try decoder.decode(MarvelStories.self, from: jsonData)
                    if let stories = jsonPayload.data?.storiesArray {
                        completion(stories)
                    }
                } catch {
                    print("There was an error parsing JSON Stories: \(error.localizedDescription)")
                }
            }
        }
        task.resume()
    }
    
    //Hero
    func downloadHeroDetails(forHeroID heroID: Int, completion: @escaping (HeroDetails)->() )  {
        //Mostra o Loader caso já não esteja ativo
        isDowloadingJSON = true
            if !isDowloadingImage {
                DispatchQueue.main.async {
                    Loader.instance.showLoader()
                }
            }
        let url = prepareURL(fetchType: .hero, forHeroID: heroID)
        
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            if let jsonData = data {
                let decoder = JSONDecoder()
                do {
                    let jsonPayload = try decoder.decode(MarvelHero.self, from: jsonData)
                    if let details = jsonPayload.data?.heroDetails! {
                        
                        completion(details[0])
                    }
                } catch {
                    print("There was an error parsing JSON Hero Details: \(error.localizedDescription)")
                }
            }
        }
        task.resume()
    }
    
    //Search
    func downloadSearchData(searchingText query: String, withOffSet offset: Int, completion: @escaping ([Character])->() )  {
        //Mostra o Loader caso já não esteja ativo
        isDowloadingJSON = true
        if !isDowloadingImage {
            DispatchQueue.main.async {
                Loader.instance.showLoader()
            }
        }
        let url = prepareURL(fetchType: .search, withOffSet: offset, searchingText: query)
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            if let jsonData = data {
                let decoder = JSONDecoder()
                
                let jsonPayload: Marvel!
                do {
                    jsonPayload = try decoder.decode(Marvel.self, from: jsonData)
                    if let filtered = jsonPayload.data?.characters {
                        self.totalEntries = (jsonPayload.data?.total!)!
                        //atualiza o fetchedIndex para armazenar quantos personagens já foram baixados
                        if offset == 0 {
                            self.fetchedIndex = 20
                        } else {
                            self.fetchedIndex += 20
                        }
                        //Faz uma chamada à completion handler
                        completion(filtered)
                    }
                } catch {
                    print("There was an error parsing JSON Search: \(error.localizedDescription)")
                }
            }
        }
        task.resume()
    }
    
    //Standard
    func downloadData(offset: Int, completion: @escaping ([Character])->() )  {
        //Mostra o Loader caso já não esteja ativo
        isDowloadingJSON = true
        if !isDowloadingImage {
            DispatchQueue.main.async {
                Loader.instance.showLoader()
            }
        }
        let url = prepareURL(fetchType: .standard, withOffSet: offset)
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            if let jsonData = data {
                let decoder = JSONDecoder()
                
                let jsonPayload: Marvel!
                do {
                    jsonPayload = try decoder.decode(Marvel.self, from: jsonData)
                    if jsonPayload.data?.total != nil {
                        self.totalEntries = (jsonPayload.data?.total!)!
                        
                        if let characters = jsonPayload.data?.characters {
                            //atualiza o fetchedIndex para armazenar quantos personagens já foram baixados
                            if offset == 0 {
                                self.fetchedIndex = 20
                            } else {
                                self.fetchedIndex += 20
                            }
                            //Faz uma chamada à completion handler
                            completion(characters)
                        }
                        
                    } else {
                        print("There was an error parsing Json Characters!")
                    }
                    
                    
                } catch {
                    print("There was an error parsing JSON Standard: \(error.localizedDescription)")
                }
            }
        }
        task.resume()
    }
    
}
