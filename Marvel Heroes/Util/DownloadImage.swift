//
//  DownloadImage.swift
//  Marvel Heroes
//
//  Created by André Grillo on 21/07/2019.
//  Copyright © 2019 André Grillo. All rights reserved.
//

import UIKit

extension UIImageView {
    func downloaded(from url: URL, heroID: String = "", isDescription: Bool = false, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        
        isDowloadingImage = true
        //Mostra o Loader caso já não esteja visível
        if isDowloadingJSON == false {
            DispatchQueue.main.async {
                Loader.instance.showLoader()
            }
        }
        
        DispatchQueue.main.async {
            self.contentMode = mode
        }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            
            //Grava a imagem para reutilização (Cache)
            if isDescription {
                if heroID != "" {
                    let dir = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0] as String + "/\(heroID)-description.jpg"
                    try? image.jpegData(compressionQuality: 1.0)?.write(to: URL(fileURLWithPath: dir))
                }
            } else {
                if heroID != "" {
                    let dir = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0] as String + "/\(heroID).jpg"
                    try? image.jpegData(compressionQuality: 1.0)?.write(to: URL(fileURLWithPath: dir))
                }
            }
            
            
            //Verifica se há algum download em andamento, se não houver retira o Loader
            isDowloadingImage = false
            DispatchQueue.main.async() {
                self.image = image
                if isDowloadingJSON == false {
                    Loader.instance.hideLoader()
                }
            }
            }.resume()
        
    }
    func downloaded(from link: String, heroID: String = "", isDescription: Bool = false, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, heroID: heroID, isDescription: isDescription, contentMode: mode)
    }
}
