//
//  Loader.swift
//  Marvel Heroes
//
//  Created by André Grillo on 22/07/2019.
//  Copyright © 2019 André Grillo. All rights reserved.
//

import UIKit

class Loader: UIView {
    
    static let instance = Loader()
    
    lazy var transparentView: UIView = {
        let transparentView = UIView(frame: UIScreen.main.bounds)
        transparentView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        transparentView.isUserInteractionEnabled = false
        return transparentView
    }()
    
    lazy var loader: UIActivityIndicatorView = {
        let loader = UIActivityIndicatorView(frame: UIScreen.main.bounds)
        loader.style = .whiteLarge
        loader.center = transparentView.center
        loader.isUserInteractionEnabled = false
        
        return loader
    }()
    
    lazy var loadingLbl: UILabel = {
        let loadingLbl = UILabel(frame: CGRect(x: UIScreen.main.bounds.width/2 - 100, y: UIScreen.main.bounds.height/2-180, width: 200, height: 200))
        loadingLbl.textColor = UIColor.white
        loadingLbl.font = UIFont(name:"Avenir Next", size: 25.0)
        loadingLbl.textAlignment = .center
        loadingLbl.numberOfLines = 2
        loadingLbl.lineBreakMode = .byWordWrapping
        loadingLbl.text = "Loading..."
        return loadingLbl
    }()
    
    func showLoader() {
        
        if isDowloadingJSON {
            loadingLbl.text = "Fetching Data..."
        } else if isDowloadingImage {
            loadingLbl.text = "Downloading\nImages..."
        } else {
            loadingLbl.text = "Loading..."
        }
        
        self.addSubview(transparentView)
        self.loader.startAnimating()
        self.transparentView.addSubview(loader)
        self.transparentView.bringSubviewToFront(self.loader)
        self.transparentView.addSubview(loadingLbl)
        self.transparentView.bringSubviewToFront(loadingLbl)
        UIApplication.shared.keyWindow?.addSubview(transparentView)
        
    }
    
    func hideLoader() {
        self.transparentView.removeFromSuperview()
    }
}
