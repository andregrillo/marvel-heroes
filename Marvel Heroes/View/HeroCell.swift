//
//  HeroCell.swift
//  Marvel Heroes
//
//  Created by André Grillo on 21/07/2019.
//  Copyright © 2019 André Grillo. All rights reserved.
//

import UIKit

class HeroCell: UICollectionViewCell {
    
    @IBOutlet weak var heroNameLbl: UILabel!
    @IBOutlet weak var heroImg: UIImageView!
    @IBOutlet weak var favoriteImg: UIImageView!
    var hero: Character!
    
    //Rounded Corners
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.cornerRadius = 5.0
    }
    
    func configureCell(hero: Character){
        heroNameLbl.text = hero.name?.capitalized
        
        //Prepara o GestureRecognizer para a imagem de favorito(envia o heroID)
        let tap = TapGesture(target: self, action: #selector(self.favoriteTapped))
        if hero.id != nil {
            tap.heroID = hero.id!
        } else {
            tap.heroID = 0
        }
        
        //Prepara o tap para o favoriteImg
        favoriteImg.addGestureRecognizer(tap)
        
        //Verifica se o herói está no array de Favoritos e configura a imagem de acordo
        if let _ = favorites.firstIndex(where: { $0 == hero.id }) {
            favoriteImg.image = UIImage(named: "favoriteOn")
        } else {
            favoriteImg.image = UIImage(named: "favoriteOff")
        }
        
        //Configura Imagem
        //verifica se há imagem disponível
        if hero.thumbnail!["path"]!.contains("image_not_available") || hero.thumbnail!["path"] == nil || hero.thumbnail!["path"] == "" {
            heroImg.image = UIImage(named: "noImage")
        } else {
            //Usa o FileManager para verificar se a imagem está no "cache"
            let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
            let url = NSURL(fileURLWithPath: path)
            if let pathComponent = url.appendingPathComponent("\(hero.id!).jpg") {
                let filePath = pathComponent.path
                let fileManager = FileManager.default
                
                //Verifica se a imagem já foi transferida. Se sim, será reutilizada.
                if fileManager.fileExists(atPath: filePath) {
                    heroImg.image = UIImage(contentsOfFile: "\(path)/\(hero.id!).jpg")
                }
                
                else {
                    //Imagem não está no "Cache". A fazer então o download...
                    let imageStringURL = "\(hero.thumbnail!["path"]!)/standard_medium.\(hero.thumbnail!["extension"]!)"
                    heroImg.downloaded(from: imageStringURL, heroID: String(hero.id!))
                }
                
            } else {
                print("File Path not available")
            }
        }
    }
    
    @objc private func favoriteTapped(sender: TapGesture){
        
        let heroID = sender.heroID
        
        if let favoriteArrayIndex = favorites.firstIndex(of: heroID) {
            favorites.remove(at: favoriteArrayIndex)
            favoriteImg.image = UIImage(named: "favoriteOff")
            //Salva as novas informações no UserDefaults
            let defaults = UserDefaults.standard
            defaults.set(favorites, forKey: "Favorites")
        } else {
            favorites.append(heroID)
            favoriteImg.image = UIImage(named: "favoriteOn")
            //Salva as novas informações no UserDefaults
            let defaults = UserDefaults.standard
            defaults.set(favorites, forKey: "Favorites")
        }
        
    }
    
    class TapGesture: UITapGestureRecognizer {
        var heroID = 0
    }
    
}
